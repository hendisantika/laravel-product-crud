# Laravel CRUD Products
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/laravel-product-crud.git`.  
2. Go inside the folder: `cd laravel-product-crud`.
3. Run `composer install`
4. Run `cp .env.example .env` & set your desired db name on it. 
3. Run `php artisan key:generate`
4. Run `php artisan migrate`
5. Run `php artisan db:seed`
6. Run `php artisan serve`

### Screen shot
Show Index Page

![Show Index Page](img/list.png "Show Index Page")

Add new Product

![Add new Product](img/add.png "Add new Product")

List All Products

![List All Products](img/list2.png "List All Products")

Show details page

![Show details page](img/details.png "Show details page")

Show Edit Page

![Show Edit Page](img/edit.png "Show Edit Page")

### Heroku Link

You can open this repository in heroku --> https://laravel-product-crud.herokuapp.com/products
